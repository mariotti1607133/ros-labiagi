#pragma once
#include "orazio_globals.h"

void Orazio_servoInit(void);

void Orazio_servoControl(void);

void Orazio_servoHandle(void);
