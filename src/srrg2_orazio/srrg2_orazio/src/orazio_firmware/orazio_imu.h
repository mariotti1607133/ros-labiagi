#pragma once
#include "orazio_globals.h"

void Orazio_imuInit(void);

void Orazio_imuControl(void);

void Orazio_imuHandle(void);
